<?php
session_start();
?>
<!doctype html>
<html lang = "pt-br">
    <head>
        <meta charset = "utf-8">
        <title> Loja </title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php
        include './banco/conexao.php';     // Conexão com o Banco
        $consulta = $conexao->query('SELECT * FROM produtos');
        ?>
        <?php include './template/nav.php'; ?>
        <?php include './template/cabecalho.html'; ?>
        
        <!-- FOOD MENU DE SELEÇÃO -->
        <section class="align-items-center bg-img bg-img-fixed" id="food-menu-section"
                 style="background-color: white;">
            <div class="container">
                    <img src="assets/logoJeh1.png" alt="Imagem da Logo" width= 200px>
                
                    <div class="food-menu">
                    <h1>
                        O que você <span class="primary-color">vai</span> comer hoje?
                    </h1>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque alias aliquam eveniet, iure
                        praesentium dicta ex dolorum inventore itaque minus repudiandae, odio provident? Velit architecto
                        natus expedita non? Odio, dolorum.
                    </p>
                    <div class="food-category">
                        <div class="zoom play-on-scroll">
                            <button class="active" data-food-type="all">
                                Todos
                            </button>
                        </div>
                        <?php
                        $consulta_categorias = $conexao->query("SELECT codigo,descricao from categoria");
                        while ($exibe_categorias = $consulta_categorias->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <div class="zoom play-on-scroll delay-2">
                                <button data-food-type="categoria<?php echo $exibe_categorias['codigo']; ?>">
                                    <?php echo $exibe_categorias['descricao']; ?>
                                </button>
                            </div>
                        <?php } ?>  
                    </div>
                    <div class="food-item-wrap all">
                        <?php
                        while ($exibir = $consulta->fetch(PDO::FETCH_ASSOC)) {  // Enquanto tiver produtos vai criando as divs
                            ?>                    
                            <div class="food-item categoria<?= $exibir['cod_categoria']; ?>-type">
                                <div class="item-wrap bottom-up play-on-scroll">
                                    <div class="item-img">
                                        <a href="./produto/informacao.php?id=<?php echo $exibir['id']; ?>">
                                            <div class="img-holder bg-img"style="background-image: url('/sistema/public/upload/<?php echo $exibir['foto1']; ?>');"></div>
                                        </a>
                                    </div>
                                    <div class="item-info">
                                        <div>
                                            <h3>
                                                <?php echo mb_strimwidth($exibir['produto'], 0, 13, '...'); ?>
                                            </h3>
                                            <span>
                                                R$ <?php echo number_format($exibir['preco'], 2, ',', '.'); ?>
                                            </span>
                                        </div>
                                        <?php if ($exibir['quantidade'] > 0) { ?>
                                            <a href = "carrinho.php?id=<?php echo $exibir['id']; ?>" class="cart-btn">
                                                <i class="bx bx-cart-alt"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a href = "#" class="cart-btn" disabled>
                                                <i class="bx bx-cart-alt"></i> Indisponível
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>                        
                    </div>
                </div>
            </div>
        </section>
        <!-- FIM DO MENU FOOD -->
        
        <!-- END TESTIMONIAL SECTION -->
        <?php include './template/rodape.html'; ?>
        <script src="index.js"></script>
    </body>
</html>