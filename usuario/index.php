<?php
session_start();
if (empty($_SESSION['adm']) || $_SESSION['adm'] != 1) {
    header('location:/sistema/index.php');
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Área Administrativa</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">
    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        $id_user = $_SESSION['id'];
        $consulta_venda = $conexao->query("SELECT * from vendas where id_comprador = '$id_user' GROUP BY ticket ORDER BY status, data desc");
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <header class="header bg-light dker bg-gradient text-right">
                        <p class="pull-left">MEUS PEDIDOS</p>
<!--                        <a href='inserir.php' class="btn btn-primary"><span class= "fa fa-pencil"></span> Cadastrar </a>-->
                    </header>
                    <section class="scrollable wrapper">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>DATA</th>
                                    <th>TICKET</th>
                                    <th>STATUS</th>
                                    <th class="text-right">
                                        <i class="fal fa-cogs"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($exibe_venda = $consulta_venda->fetch(PDO::FETCH_ASSOC)) {
// Verificando Status do Pedido
                                    $status_pedido = $exibe_venda['status'];
                                    if ($status_pedido == 'C') {
                                        $status_pedido = 'Confirmado';
                                    } elseif ($status_pedido == 'A') {
                                        $status_pedido = 'Aguardando Confirmação';
                                    } elseif ($status_pedido == 'E') {
                                        $status_pedido = 'Entregue';
                                    }
// Fim da verificação do Status
                                    $consulta_Prod = $conexao->query("SELECT produto from produtos where id= '$exibe_venda[id_produto]'");
                                    $exibe_Prod = $consulta_Prod->fetch(PDO::FETCH_ASSOC);
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d/m/Y', strtoTime($exibe_venda['data'])); ?>
                                        </td>
                                        <td>
                                            <?php echo $exibe_venda['ticket']; ?>
                                        </td>
                                        <td>
                                            <?php echo $status_pedido; ?>
                                        </td>
                                        <td class="text-right">
                                            <a href="../ticket.php?ticket=<?php echo $exibe_venda['ticket']; ?>" class="btn btn-default"><i class="fa fa-search"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </section>
                </section>
            </section>
        </section>
        <script src="/sistema/public/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/sistema/public/js/bootstrap.js"></script>
        <!-- App -->
        <script src="/sistema/public/js/app.js"></script>
        <script src="/sistema/public/js/app.plugin.js"></script>
        <script src="/sistema/public/js/app.data.js"></script>
        <!-- Fuelux -->
        <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    </body>
</html>
