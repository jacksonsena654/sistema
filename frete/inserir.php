<?php
session_start();
if (empty($_SESSION['adm']) || $_SESSION['adm'] != 1) {
    header('location:/sistema/index.php');
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Cadastro de Categoria</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="../style.css">
        <script src="../jquery.mask.js"></script>
        <script>
            $(document).ready(function () {
                $('#preco').mask('000.000.000.000.000,00', {reverse: true});
            });
        </script>
        <style>
            .navbar{
                margin-bottom: 0;
            }
        </style>
    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        include '../template/nav.php';
        ?>
        <section class="align-items-center bg-img bg-img-fixed" id="food-menu-section" style="background-image: url(assets/katherine-chase-4MMK78S7eyk-unsplash.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <h2>Inclusão de Categorias</h2>
                        <form method="post" action="svInserir.php" name="incluiCategoria" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input name="nome" type="text" class="form-control" required id="nome" style="text-transform: uppercase;">
                            </div>
                            <button type="submit" class="btn btn-lg btn-default">
                                <span class="glyphicon glyphicon-pencil"> Cadastrar </span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <?php include '../template/rodape.html'; ?>
    </body>
</html>