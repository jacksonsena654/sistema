<?php
session_start();
if (empty($_SESSION['id'])) {
    header('location:login.php');
}
?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Carrinho</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>	
        <section class="align-items-center bg-img bg-img-fixed" id="food-menu-section" style="background-image: url(assets/katherine-chase-4MMK78S7eyk-unsplash.jpg);">
            <?php
            include './banco/conexao.php';
            include './template/nav.php';
            if (!empty($_GET['id'])) {
                $id_prod = $_GET['id'];
                $subtrair = false;
                if (!empty($_GET['subtrair'])) {
                    $subtrair = $_GET['subtrair'];
                }
                if (!isset($_SESSION['carrinho'])) {
                    $_SESSION['carrinho'] = array();
                }
                if (!isset($_SESSION['carrinho'][$id_prod])) {
                    $_SESSION['carrinho'][$id_prod] = 1;
                } else if (isset($_SESSION['carrinho'][$id_prod]) && $subtrair) {
                    $_SESSION['carrinho'][$id_prod] -= 1;
                } else {
                    $_SESSION['carrinho'][$id_prod] += 1;
                }
                include 'mostraCarrinho.php';
            } else {
                include 'mostraCarrinho.php';
            }
            ?>
            <div class="row text-center" style="margin-top: 15px;">
                <h1>Total: R$ <?php echo number_format($total, 2, ',', '.'); ?> </h1>
            </div>
            <div class="row text-center" style="margin-top: 15px;">
                <a href="index.php"><button class="btn btn-lg btn-primary">Continuar Comprando</button></a>
                <?php if (count($_SESSION['carrinho']) > 0) { ?>
                    <a href="finalizarCompra.php"><button class="btn btn-lg btn-success">Finalizar Compra</button></a>
                <?php } ?>
            </div>
        </section>
        <?php
        include './template/rodape.html';
        ?>
    </body>
</html>