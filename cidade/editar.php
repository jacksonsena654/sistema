<?php
session_start();
if (empty($_SESSION['adm']) || $_SESSION['adm'] != 1) {
    header('location:/sistema/index.php');
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Área Administrativa</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">

    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        $id_cidade = $_GET['id'];
        $consulta = $conexao->query("SELECT c.nome,e.sigla FROM cidade c left outer join estado e on e.id_estado = c.id_estado WHERE c.id_cidade='$id_cidade'");
        $exibe = $consulta->fetch(PDO::FETCH_ASSOC)
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <form method="post" action="svEditar.php?id=<?php echo $id_cidade; ?>" name="alteraProd" enctype="multipart/form-data">
                        <header class="header bg-light dker bg-gradient text-right">
                            <p class="pull-left">Alterar Cidade</p>
                            <button type="submit" class="btn btn-primary">
                                <span class="fa fa-save"> Salvar </span>
                            </button>
                        </header>
                        <section class="scrollable wrapper">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group " title="">
                                        <div class="form-group">
                                            <label class="control-label" for="nome">Nome</label>
                                            <input type="text" name="nome" value="<?php echo $exibe['nome']; ?>"  class="form-control" required id="nome" style="text-transform: uppercase;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="UF">Estado</label>
                                        <select class="form-control" name="UF" id="UF" required>
                                            <?php
                                            $consulta_cidades = $conexao->query("SELECT id_estado,nome,sigla from estado");
                                            while ($exibe_cidades = $consulta_cidades->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option value="<?php echo $exibe_cidades['sigla']; ?>"  <?= ($exibe['sigla'] == $exibe_cidades['sigla']) ? 'selected' : '' ?>><?php echo $exibe_cidades['nome']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </section>
            </section>
        </section>
    </section>
    <script src="/sistema/public/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/sistema/public/js/bootstrap.js"></script>
    <!-- App -->
    <script src="/sistema/public/js/app.js"></script>
    <script src="/sistema/public/js/app.plugin.js"></script>
    <script src="/sistema/public/js/app.data.js"></script>
    <!-- Fuelux -->
    <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    <script src="../jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('#preco').mask('000.000.000.000.000,00', {reverse: true});
        });
    </script>
</body>
</html>
