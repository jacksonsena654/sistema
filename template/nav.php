<!-- MOBILE NAV -->

<!-- FIM MOBILE NAV -->
<!-- BACK DO BTN TOP -->
<a href="#home" class="back-to-top">
    <i class="bx bxs-to-top"></i>
</a>
<!-- BACK END DO BTN TOP -->
<!-- NAVEGAÇÃO TOP -->
<div class="nav">
    <div class="menu-wrap">
        <a href="/sistema/index.php">
            <div class="logo">
                <img src="assets/logoJeh1.png" alt="" width=60px>
            </div>
        </a>
        <div class="menu h-xs">
            <a href="#home">
        
            </a>
            <?php if (empty($_SESSION['id'])) { ?>
                <a href="/sistema/login.php">
                </a>
                <?php
            } else {
                $consulta_user = $conexao->query("SELECT nome,adm FROM usuarios WHERE id_cliente='$_SESSION[id]'");
                $exibe_user = $consulta_user->fetch(PDO::FETCH_ASSOC);
                if ($exibe_user['adm'] == 0) { // se n for um Administrador
                    ?>
                    <a href="/sistema/usuario/index.php">
                        <div class="menu-item">
                            <?php echo $exibe_user['nome']; ?>
                        </div>
                    </a>
                    <a href="/sistema/sair.php">
                        <div class="menu-item">
                            Sair
                        </div>
                    </a>
                <?php } else { ?>
                    <a href="/sistema/adm/adm.php">
                        <div class="menu-item">
                            ADMINISTRAÇÃO
                        </div>
                    </a>
                    <a href="/sistema/sair.php">
                        <div class="menu-item">
                            Sair
                        </div>
                    </a>
                    <?php
                }
            }
            ?>
        </div>
        <div class="right-menu">
            <a href="/sistema/carrinho.php" class="cart-btn">
                <i class='bx bx-cart-alt'></i>
            </a>
        </div>
    </div>
</div>
<!-- FIM DA NAVEGAÇÃO TOP -->