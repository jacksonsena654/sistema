<?php
header("Content-Type:application/json;");
if (isset($_GET['id']) && $_GET['id'] != "") {
    include './banco/conexao.php';
    if($_GET['id'] == 'all'){
        $consulta = $conexao->query('SELECT * FROM produtos');
        $produto = $consulta->fetchAll(PDO::FETCH_ASSOC);
        if ($produto) {
            response($produto, null, null);
        } else {
            response(array(), 200, "Nada Encontrado");
        }
    } else {
        $id = $_GET['id'];
        if(!is_numeric($id)){
            response(array(), 400, "Digite um id valido!");exit();
        }
        $consulta = $conexao->query('SELECT * FROM produtos WHERE id=' . $id);
        $produto = $consulta->fetch(PDO::FETCH_ASSOC);
        if ($produto) {
            response($produto, null, null);
        } else {
            response(array(), 200, $id. " Nada Encontrado");
        }
    }
} else {
    response(array(), 400, "Requisição Inválida");
}

function response($array, $response_code, $response_desc) {
    $response['codigo'] = $response_code;
    $response['descricao'] = $response_desc;
    $json_response = json_encode(array_merge($response, $array));
    echo $json_response;
}
?>