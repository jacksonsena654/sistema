<?php
session_start();
if (empty($_SESSION['adm']) || $_SESSION['adm'] != 1) {
    header('location:/sistema/index.php');
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Área Administrativa</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">

    </head>
    <body>
        <?php
        $id_prod = $_GET['id'];
        include '../banco/conexao.php';
        $consulta = $conexao->query("SELECT * FROM produtos WHERE id='$id_prod'");
        $exibe = $consulta->fetch(PDO::FETCH_ASSOC)
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <form method="post" action="svEditar.php?id=<?php echo $id_prod; ?>" name="alteraProd" enctype="multipart/form-data">
                        <header class="header bg-light dker bg-gradient text-right">
                            <p class="pull-left">Alterar Produto</p>
                            <button type="submit" class="btn btn-primary">
                                <span class="fa fa-save"> Salvar </span>
                            </button>
                        </header>
                        <section class="scrollable wrapper">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group " title="">
                                        <label class="control-label" for="nome">Produto *</label>                                
                                        <input type="text" name="produto" value="<?php echo $exibe['produto']; ?>"  class="form-control" required id="produto">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label class="control-label"for="departamento">Tipo *</label>                                    
                                        <select class="form-control" name="departamento">
                                            <option value="">Selecione</option>
                                            <?php
                                            $consulta_categorias = $conexao->query("SELECT codigo,descricao from categoria");
                                            while ($exibe_categorias = $consulta_categorias->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option value="<?php echo $exibe_categorias['codigo']; ?>"  <?= ($exibe['cod_categoria'] == $exibe_categorias['codigo']) ? 'selected' : '' ?>><?php echo $exibe_categorias['descricao']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>                                
                                    </div>
                                </div>                               
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="quantidade">Quantidade</label>
                                        <input type="number" class="form-control" required name="quantidade" value="<?php echo $exibe['quantidade']; ?>" id="quantidade">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="preco">Preço</label>
                                        <input type="text" class="form-control" required name="preco" value="<?php echo $exibe['preco']; ?>" id="preco">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="descricao">Descrição</label>
                                        <textarea rows="5" class="form-control" name="descricao"><?php echo $exibe['descricao']; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="foto1">Foto Principal</label>
                                        <input type="file" accept="image/*" class="form-control" name="foto1" id="foto1">
                                    </div>
                                
                                    <div class="form-group">
                                        <img src="/sistema/public/upload/<?php echo $exibe['foto1']; ?>" width="100px" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="foto2">Foto 2</label>
                                        <input type="file" accept="image/*" class="form-control"  name="foto2" id="foto2">
                                    </div>
                                
                                    <div class="form-group">
                                        <img src="/sistema/public/upload/<?php echo $exibe['foto2']; ?>" width="100px" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="foto2">Foto 3</label>
                                        <input type="file" accept="image/*" class="form-control"  name="foto3" id="foto3">
                                    </div>
                                
                                    <div class="form-group">
                                        <img src="/sistema/public/upload/<?php echo $exibe['foto3']; ?>" width="100px" >
                                    </div>
                                </div>
                                
                            </div>
                        </section>
                    </form>
                </section>
            </section>
        </section>

    </section>
    <script src="/sistema/public/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/sistema/public/js/bootstrap.js"></script>
    <!-- App -->
    <script src="/sistema/public/js/app.js"></script>
    <script src="/sistema/public/js/app.plugin.js"></script>
    <script src="/sistema/public/js/app.data.js"></script>
    <!-- Fuelux -->
    <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    <script src="../jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('#preco').mask('000.000.000.000.000,00', {reverse: true});
        });
    </script>
</body>
</html>
